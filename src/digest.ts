import { createHash, type Encoding } from 'node:crypto';
import { type ClientRequest, type IncomingMessage, type RequestOptions, request, type InformationEvent } from 'node:http';
import { Socket } from 'node:net';
import * as stream from 'node:stream';

export class DigestClient {
    username: string;
    password: string;
    nonceCount: number = 1;
    private _request: ClientRequest;
    private realm: string;
    private nonce: string;
    private algorithm: string;
    private qop: string[];
    private opaque: string;
    private stale: Boolean;
    private userhash: Boolean;
    private charset: Encoding;
    constructor(username: string, password: string) {
        this.username = username.replaceAll('"', '');
        this.password = password;
    }
    request(url: string | URL, options: RequestOptions, callback?: (res: IncomingMessage) => void): ClientRequest;
    request(options: string | RequestOptions | URL, callback?: (res: IncomingMessage) => void): ClientRequest;
    request(urlOrOptions?: string | URL | RequestOptions, optionsOrCallback?: RequestOptions | ((res: IncomingMessage) => void), callback?: (res: IncomingMessage) => void): ClientRequest {
        const $this = this;
        let callbackRouter = (res: IncomingMessage) => {
            let currentCallback: (res: IncomingMessage) => void;
            let currentOptions: RequestOptions;
            if (typeof optionsOrCallback == 'function') {
                currentCallback = optionsOrCallback;
            } else if (typeof callback == 'function') {
                currentCallback = callback;
            }
            if (optionsOrCallback && typeof optionsOrCallback != 'function') {
                currentOptions = optionsOrCallback;
            } else if (urlOrOptions && typeof urlOrOptions != 'string' && !(urlOrOptions instanceof URL)) {
                currentOptions = urlOrOptions;
            }
            if (res.statusCode == 401) {
                let url: string | URL;
                if (typeof urlOrOptions == 'string' || urlOrOptions instanceof URL) {
                    url = urlOrOptions;
                }
                $this._handle401(url, res, currentOptions, currentCallback);
            } else if (currentCallback) {
                currentCallback(res);
            }
        }
        if (typeof urlOrOptions == 'string' || urlOrOptions instanceof URL) {
            if (typeof optionsOrCallback != 'function') {
                $this._request = request(urlOrOptions, optionsOrCallback, callbackRouter);
                return $this._request;
            }
        }
        $this._request = request(urlOrOptions, callbackRouter);
        return $this._request;
    }
    private _genEntityBody(res: IncomingMessage) {
        return new Promise<any>((resolve, reject) => {
            let entityBody: any;
            let dataHandler = () => {
                res.setEncoding('utf-8');
                entityBody = Object.entries(res.headers).map(v => v.join(': ')).join('\r\n');
                entityBody += '\r\n';
                let chunk: string;
                while (null !== (chunk = res.read())) {
                    entityBody += res.read();
                }
            };
            res.on('readable', dataHandler);
            res.on('end', () => {
                res.off('data', dataHandler);
                entityBody += '\r\n';
                entityBody += res.rawTrailers.join('\r\n');
                resolve(entityBody);
            });
            res.on('close', () => {
                res.off('data', dataHandler);
                resolve(entityBody);
            });
            res.on('error', (err) => {
                res.off('data', dataHandler);
                reject(err);
            })
        })
    }
    private _handle401(url: string | URL, res: IncomingMessage, options?: RequestOptions, callback?: (res: IncomingMessage) => void) {
        // let header = this.parseWWWAuthenticateHeader(res.headers['www-authenticate']);
        const $this = this;
        $this._genDigestAuth(res.headers['www-authenticate'], options?.method || 'GET', url.toString(), res).then(digestStr => {
            let _request = $this.request.bind($this);
            // if ($this.stale) {
            //     _request = request;
            // }
            let headers = {
                ...options?.headers,
                Authorization: digestStr
            }
            if (url) {
                $this._request = _request(url, { ...options, headers }, callback);
            } else {
                $this._request = _request({ ...options, headers }, callback);
            }
            $this.end();
        });
    }
    private async _genDigestAuth(authStr: string, method: string, digestURI: string, res: IncomingMessage): Promise<string> {
        try {
            let ha1: string, ha2: string, response: string, authParams: URLSearchParams, nc: string, cnonce: string, digestStr: string, entityBody: string, domain: string, stale: string, alg: string, userhash: string, selectedQop: string, digestObj: Record<string, string>;
            authParams = new URLSearchParams(authStr?.replace('Digest ', '')?.replaceAll(', ', '&')?.replaceAll('"', ''));
            this.realm = authParams.get('realm');
            domain = authParams.get('domain');
            this.nonce = authParams.get('nonce');
            this.opaque = authParams.get('opaque');
            stale = authParams.get('stale');
            this.stale = stale ? stale?.toLowerCase() != 'false' : false;
            this.algorithm = authParams.get('algorithm');
            alg = this.algorithm.toLowerCase().replace('-sess', '').replace('-', '');
            this.qop = authParams.get('qop').split(',') || ['auth'];
            this.charset = (authParams.get('charset')?.toLowerCase() || 'utf-8') as Encoding;
            userhash = authParams.get('userhash');
            this.userhash = userhash ? userhash.toLowerCase() == 'true' : false;
            if (this.userhash) {
                this.username = createHash(alg).update(this.username, this.charset).digest("hex");
            }
            cnonce = new Date().valueOf().toString(16);
            nc = (this.nonceCount++).toString(16).padStart(8, '0');
            ha1 = createHash(alg).update([this.username, this.realm, this.password].join(':'), this.charset).digest("hex");
            if (this.algorithm.match(/-sess$/)) {
                ha1 = createHash(alg).update([ha1, this.nonce, cnonce].join(':'), this.charset).digest("hex");
            }
            selectedQop = 'auth';
            if (this.qop.includes('auth-int')) {
                selectedQop = 'auth-int';
                entityBody = createHash(alg).update((await this._genEntityBody(res)), this.charset).digest("hex");
                ha2 = createHash(alg).update([method.toUpperCase(), digestURI, entityBody].join(':'), this.charset).digest("hex");
            } else {
                ha2 = createHash(alg).update([method.toUpperCase(), digestURI].join(':'), this.charset).digest("hex");
            }
            response = ['"', createHash(alg).update([ha1, this.nonce, nc, cnonce, selectedQop, ha2].join(':'), this.charset).digest("hex"), '"'].join('');
            digestObj = {
                response,
                username: `"${this.username}"`,
                realm: `"${this.realm}"`,
                uri: `"${digestURI}"`,
                qop: selectedQop,
                cnonce: `"${cnonce}"`,
                nc,
                userhash: `"${this.userhash}"`,
                nonce: `"${this.nonce}"`,
                algorithm: this.algorithm,
            };
            if (false && !this.userhash) {
                /* Need to check for ABNF compliance in username; if this.userhash if false, store this.username in digestObj['username*'] using unquoted extended notation [RFC5987] */
                delete digestObj.username;
                digestObj['username*'] = this.username;
            }
            if (this.opaque) {
                digestObj.opaque = `"${this.opaque}"`;
            }
            digestStr = `Digest ${Object.entries(digestObj).map(v => v.join('=')).join(', ')}`;
            return digestStr;
        } catch (e) {
            throw e;
        }
    }
    end(chunk: any, encoding: BufferEncoding, cb?: () => void): ClientRequest
    end(chunk: any, cb?: () => void): ClientRequest
    end(cb?: () => void): ClientRequest;
    end(chunkOrCb?: any | (() => void), encodingOrCB?: BufferEncoding | (() => void), cb?: () => void): ClientRequest {
        if (cb) {
            return this._request.end(chunkOrCb as any, encodingOrCB as BufferEncoding, cb)
        } else if (encodingOrCB) {
            return this._request.end(chunkOrCb as any, encodingOrCB as () => void)
        }
        return this._request.end(chunkOrCb)
    }

    /**
     * The `request.aborted` property will be `true` if the request has
     * been aborted.
     * @since v0.11.14
     * @deprecated Since v17.0.0,v16.12.0 - Check `destroyed` instead.
     */
    aborted: boolean;
    /**
     * The request host.
     * @since v14.5.0, v12.19.0
     */
    host: string;
    /**
     * The request protocol.
     * @since v14.5.0, v12.19.0
     */
    protocol: string;
    /**
     * When sending request through a keep-alive enabled agent, the underlying socket
     * might be reused. But if server closes connection at unfortunate time, client
     * may run into a 'ECONNRESET' error.
     *
     * ```js
     * const http = require('node:http');
     *
     * // Server has a 5 seconds keep-alive timeout by default
     * http
     *   .createServer((req, res) => {
     *     res.write('hello\n');
     *     res.end();
     *   })
     *   .listen(3000);
     *
     * setInterval(() => {
     *   // Adapting a keep-alive agent
     *   http.get('http://localhost:3000', { agent }, (res) => {
     *     res.on('data', (data) => {
     *       // Do nothing
     *     });
     *   });
     * }, 5000); // Sending request on 5s interval so it's easy to hit idle timeout
     * ```
     *
     * By marking a request whether it reused socket or not, we can do
     * automatic error retry base on it.
     *
     * ```js
     * const http = require('node:http');
     * const agent = new http.Agent({ keepAlive: true });
     *
     * function retriableRequest() {
     *   const req = http
     *     .get('http://localhost:3000', { agent }, (res) => {
     *       // ...
     *     })
     *     .on('error', (err) => {
     *       // Check if retry is needed
     *       if (req.reusedSocket &#x26;&#x26; err.code === 'ECONNRESET') {
     *         retriableRequest();
     *       }
     *     });
     * }
     *
     * retriableRequest();
     * ```
     * @since v13.0.0, v12.16.0
     */
    reusedSocket: boolean;
    /**
     * Limits maximum response headers count. If set to 0, no limit will be applied.
     */
    maxHeadersCount: number;
    /**
     * The request method.
     * @since v0.1.97
     */
    method: string;
    /**
     * The request path.
     * @since v0.4.0
     */
    path: string;
    /**
     * Marks the request as aborting. Calling this will cause remaining data
     * in the response to be dropped and the socket to be destroyed.
     * @since v0.3.8
     * @deprecated Since v14.1.0,v13.14.0 - Use `destroy` instead.
     */
    abort(): void {
        this._request.abort()
    };
    onSocket(socket: Socket): void {
        this._request.onSocket(socket)
    };
    /**
     * Once a socket is assigned to this request and is connected `socket.setTimeout()` will be called.
     * @since v0.5.9
     * @param timeout Milliseconds before a request times out.
     * @param callback Optional function to be called when a timeout occurs. Same as binding to the `'timeout'` event.
     */
    setTimeout(timeout: number, callback?: () => void): ClientRequest {
        return this._request.setTimeout(timeout, callback)
    };
    /**
     * Once a socket is assigned to this request and is connected `socket.setNoDelay()` will be called.
     * @since v0.5.9
     */
    setNoDelay(noDelay?: boolean): void {
        this._request.setNoDelay(noDelay)
    };
    /**
     * Once a socket is assigned to this request and is connected `socket.setKeepAlive()` will be called.
     * @since v0.5.9
     */
    setSocketKeepAlive(enable?: boolean, initialDelay?: number): void {
        this._request.setSocketKeepAlive(enable, initialDelay)
    };
    /**
     * Returns an array containing the unique names of the current outgoing raw
     * headers. Header names are returned with their exact casing being set.
     *
     * ```js
     * request.setHeader('Foo', 'bar');
     * request.setHeader('Set-Cookie', ['foo=bar', 'bar=baz']);
     *
     * const headerNames = request.getRawHeaderNames();
     * // headerNames === ['Foo', 'Set-Cookie']
     * ```
     * @since v15.13.0, v14.17.0
     */
    getRawHeaderNames(): string[] {
        return this._request.getRawHeaderNames()
    };
    /**
     * @deprecated
     */
    addListener(event: 'abort', listener: () => void): ClientRequest;
    addListener(event: 'connect', listener: (response: IncomingMessage, socket: Socket, head: Buffer) => void): ClientRequest;
    addListener(event: 'continue', listener: () => void): ClientRequest;
    addListener(event: 'information', listener: (info: InformationEvent) => void): ClientRequest;
    addListener(event: 'response', listener: (response: IncomingMessage) => void): ClientRequest;
    addListener(event: 'socket', listener: (socket: Socket) => void): ClientRequest;
    addListener(event: 'timeout', listener: () => void): ClientRequest;
    addListener(event: 'upgrade', listener: (response: IncomingMessage, socket: Socket, head: Buffer) => void): ClientRequest;
    addListener(event: 'close', listener: () => void): ClientRequest;
    addListener(event: 'drain', listener: () => void): ClientRequest;
    addListener(event: 'error', listener: (err: Error) => void): ClientRequest;
    addListener(event: 'finish', listener: () => void): ClientRequest;
    addListener(event: 'pipe', listener: (src: stream.Readable) => void): ClientRequest;
    addListener(event: 'unpipe', listener: (src: stream.Readable) => void): ClientRequest;
    addListener(event: string | symbol, listener: (...args: any[]) => void): ClientRequest {
        return this._request.addListener(event, listener)
    };
    /**
     * @deprecated
     */
    on(event: 'abort', listener: () => void): ClientRequest;
    on(event: 'connect', listener: (response: IncomingMessage, socket: Socket, head: Buffer) => void): ClientRequest;
    on(event: 'continue', listener: () => void): ClientRequest;
    on(event: 'information', listener: (info: InformationEvent) => void): ClientRequest;
    on(event: 'response', listener: (response: IncomingMessage) => void): ClientRequest;
    on(event: 'socket', listener: (socket: Socket) => void): ClientRequest;
    on(event: 'timeout', listener: () => void): ClientRequest;
    on(event: 'upgrade', listener: (response: IncomingMessage, socket: Socket, head: Buffer) => void): ClientRequest;
    on(event: 'close', listener: () => void): ClientRequest;
    on(event: 'drain', listener: () => void): ClientRequest;
    on(event: 'error', listener: (err: Error) => void): ClientRequest;
    on(event: 'finish', listener: () => void): ClientRequest;
    on(event: 'pipe', listener: (src: stream.Readable) => void): ClientRequest;
    on(event: 'unpipe', listener: (src: stream.Readable) => void): ClientRequest;
    on(event: string | symbol, listener: (...args: any[]) => void): ClientRequest {
        return this._request.on(event, listener)
    }
    /**
     * @deprecated
     */
    once(event: 'abort', listener: () => void): ClientRequest;
    once(event: 'connect', listener: (response: IncomingMessage, socket: Socket, head: Buffer) => void): ClientRequest;
    once(event: 'continue', listener: () => void): ClientRequest;
    once(event: 'information', listener: (info: InformationEvent) => void): ClientRequest;
    once(event: 'response', listener: (response: IncomingMessage) => void): ClientRequest;
    once(event: 'socket', listener: (socket: Socket) => void): ClientRequest;
    once(event: 'timeout', listener: () => void): ClientRequest;
    once(event: 'upgrade', listener: (response: IncomingMessage, socket: Socket, head: Buffer) => void): ClientRequest;
    once(event: 'close', listener: () => void): ClientRequest;
    once(event: 'drain', listener: () => void): ClientRequest;
    once(event: 'error', listener: (err: Error) => void): ClientRequest;
    once(event: 'finish', listener: () => void): ClientRequest;
    once(event: 'pipe', listener: (src: stream.Readable) => void): ClientRequest;
    once(event: 'unpipe', listener: (src: stream.Readable) => void): ClientRequest;
    once(event: string | symbol, listener: (...args: any[]) => void): ClientRequest {
        return this._request.once(event, listener)
    };
    /**
     * @deprecated
     */
    prependListener(event: 'abort', listener: () => void): ClientRequest;
    prependListener(event: 'connect', listener: (response: IncomingMessage, socket: Socket, head: Buffer) => void): ClientRequest;
    prependListener(event: 'continue', listener: () => void): ClientRequest;
    prependListener(event: 'information', listener: (info: InformationEvent) => void): ClientRequest;
    prependListener(event: 'response', listener: (response: IncomingMessage) => void): ClientRequest;
    prependListener(event: 'socket', listener: (socket: Socket) => void): ClientRequest;
    prependListener(event: 'timeout', listener: () => void): ClientRequest;
    prependListener(event: 'upgrade', listener: (response: IncomingMessage, socket: Socket, head: Buffer) => void): ClientRequest;
    prependListener(event: 'close', listener: () => void): ClientRequest;
    prependListener(event: 'drain', listener: () => void): ClientRequest;
    prependListener(event: 'error', listener: (err: Error) => void): ClientRequest;
    prependListener(event: 'finish', listener: () => void): ClientRequest;
    prependListener(event: 'pipe', listener: (src: stream.Readable) => void): ClientRequest;
    prependListener(event: 'unpipe', listener: (src: stream.Readable) => void): ClientRequest;
    prependListener(event: string | symbol, listener: (...args: any[]) => void): ClientRequest {
        return this._request.prependListener(event, listener)
    };
    /**
     * @deprecated
     */
    prependOnceListener(event: 'abort', listener: () => void): ClientRequest;
    prependOnceListener(event: 'connect', listener: (response: IncomingMessage, socket: Socket, head: Buffer) => void): ClientRequest;
    prependOnceListener(event: 'continue', listener: () => void): ClientRequest;
    prependOnceListener(event: 'information', listener: (info: InformationEvent) => void): ClientRequest;
    prependOnceListener(event: 'response', listener: (response: IncomingMessage) => void): ClientRequest;
    prependOnceListener(event: 'socket', listener: (socket: Socket) => void): ClientRequest;
    prependOnceListener(event: 'timeout', listener: () => void): ClientRequest;
    prependOnceListener(event: 'upgrade', listener: (response: IncomingMessage, socket: Socket, head: Buffer) => void): ClientRequest;
    prependOnceListener(event: 'close', listener: () => void): ClientRequest;
    prependOnceListener(event: 'drain', listener: () => void): ClientRequest;
    prependOnceListener(event: 'error', listener: (err: Error) => void): ClientRequest;
    prependOnceListener(event: 'finish', listener: () => void): ClientRequest;
    prependOnceListener(event: 'pipe', listener: (src: stream.Readable) => void): ClientRequest;
    prependOnceListener(event: 'unpipe', listener: (src: stream.Readable) => void): ClientRequest;
    prependOnceListener(event: string | symbol, listener: (...args: any[]) => void): ClientRequest {
        return this._request.prependOnceListener(event, listener)
    };
}
