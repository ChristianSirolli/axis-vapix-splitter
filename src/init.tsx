import { Hono } from 'hono/quick';
import type { Context } from 'hono';
import { HTTPException } from 'hono/http-exception';
import { serve } from '@hono/node-server';
import { createCanvas, loadImage } from '@napi-rs/canvas';
import type { IncomingMessage } from 'http';
import { logger } from 'hono/logger';
import { createRxDatabase, addRxPlugin, toTypedRxJsonSchema } from 'rxdb';
import type { RxDatabase, RxCollection, RxJsonSchema, RxDocument, ExtractDocumentTypeFromTypedRxJsonSchema } from 'rxdb';
import { getRxStorageLoki } from 'rxdb/plugins/storage-lokijs';
import { RxDBMigrationPlugin } from 'rxdb/plugins/migration';
import LokiFSStructuredAdapter from 'lokijs/src/loki-fs-structured-adapter.js';
import { randomUUID } from 'crypto';
import { DigestClient } from './digest.js';
import { html } from 'hono/html';
import { PerformanceObserver, performance } from 'perf_hooks';
import { Worker, isMainThread, parentPort, workerData } from 'worker_threads';
const obs = new PerformanceObserver((items) => {
    let item = items.getEntries()[0];
    console.log(item.name, item.entryType, item.duration);
    performance.clearMarks();
});
obs.observe({ type: 'measure' });
if (isMainThread) {
(async () => {
    const threads = new Set();
    const app: Hono = new Hono({ strict: false });
    try {
        addRxPlugin(RxDBMigrationPlugin);

        type CameraDocument = RxDocument<CameraDocType>;
        type DBCollection = RxCollection<CameraDocType>;
        type DBCollections = {
            cameras: DBCollection
        }
        type CameraDatabase = RxDatabase<DBCollections>;

        const dbSchemaLiteral = {
            version: 2,
            primaryKey: 'id',
            type: 'object',
            properties: {
                id: { type: 'string', maxLength: 36 }, // uuid
                name: { type: 'string' },
                host: { type: 'string' },
                port: { type: 'integer' },
                make: { type: 'string' },
                model: { type: 'string' },
                maxX: { type: 'integer' },
                maxY: { type: 'integer' },
                user: { type: 'string' },
                pass: { type: 'string' },
                boundary: { type: 'string' },
                width: { type: 'number' },
                height: { type: 'number' },
                thumbnail: { type: 'string' },
                overlapx: { type: 'number' },
                overlapy: { type: 'number' },
                fps: { type: 'number', default: 30 },
            },
            required: ['id', 'host'],
        } as const;

        const schemaTyped = toTypedRxJsonSchema(dbSchemaLiteral);

        type CameraDocType = ExtractDocumentTypeFromTypedRxJsonSchema<typeof schemaTyped>;


        const db: CameraDatabase = await createRxDatabase({
            name: 'cameradb',
            storage: getRxStorageLoki({
                adapter: new LokiFSStructuredAdapter(),
            })
        });
        const dbSchema: RxJsonSchema<CameraDocType> = dbSchemaLiteral;

        await db.addCollections({
            cameras: {
                schema: dbSchema,
                migrationStrategies: {
                    1: oldDoc => {
                        oldDoc.width = null;
                        oldDoc.height = null;
                        oldDoc.user = oldDoc.user || null;
                        oldDoc.pass = oldDoc.pass || '';
                        oldDoc.thumbnail = oldDoc.thumbnail || null;
                        oldDoc.overlapx = oldDoc.overlapx || null;
                        oldDoc.overlapy = oldDoc.overlapy || null;
                        return oldDoc;
                    },
                    2: oldDoc => {
                        oldDoc.boundary = null;
                        oldDoc.fps = 30;
                        return oldDoc;
                    },
                }
            }
        });

        db.cameras.postInsert(
            function myPostInsertHook(
                this: DBCollection, // own collection is bound to the scope
                docData: CameraDocType, // documents data
                doc: CameraDocument // RxDocument
            ) {
                console.log('insert to ' + this.name + '-collection: ' + doc.id);
            },
            false // not async
        );

        process.removeAllListeners('uncaughtException');

        const incomingMessages: Record<string, IncomingMessage> = {};

        app.use('*', logger(console.log));
        function log(c: Context, level: 'debug' | 'log' | 'warn' | 'error', ...messages: any[]) {
            const queriedLevel = c.req.query('logs');
            if (!queriedLevel || (isNaN(Number(queriedLevel)) && level !== queriedLevel)) {
                return;
            }
            console[level](`${level !== 'warn' ? level : 'warning'}:`, ...messages);
            console.trace();
        }
        
        app.onError((err: Error, c: Context) => {
            log(c, 'error', err);
            if (err instanceof HTTPException) {
                return err.getResponse();
            } else {
                return c.text(err.message, 500);
            }
        });
        app.get('/mjpeg', (c: Context) => {
            log(c, 'debug', c.req.queries());
            performance.measure('Start to Now');
            return new Promise<Response>((resolve, reject) => {
                const { readable, writable } = new TransformStream();
                //     {
                //     transform(chunk, controller) {
                //         try {
                //             controller.enqueue(chunk);
                //         }
                //         catch (err) {
                //             if (err instanceof Error) {
                //                 log(c, 'error', 'Error in TransformStream:', err);
                //             }
                //         }
                //     },
                // }, new ByteLengthQueuingStrategy({ highWaterMark: 65536 * 5 }), new ByteLengthQueuingStrategy({ highWaterMark: 65536 * 5 }));
                return processData(c, writable.getWriter(), readable).then(resolve).catch(reject);
            });
        })
        async function processData(c: Context, writer: WritableStreamDefaultWriter, readable: ReadableStream): Promise<Response> {
            performance.mark('processData init');
            const id = c.req.query('id'),
                data = await db.cameras.findOne({ selector: { id } }).exec(true),
                res = incomingMessages[id];
            if (!res) {
                return c.text('Connection not established', 500);
            }
            log(c, 'log', 'PD1: Starting to process incoming message');
            const canvas = createCanvas(1920, 1080);
            const ctx = canvas.getContext('2d');
            let x = Number(c.req.query('x') || '0'),
                y = Number(c.req.query('y') || '0');
            log(c, 'log', 'PD2: Creating promise to return');
            performance.mark('processData preReturn');
            performance.measure('processData Init', 'processData init', 'processData preReturn');
            return new Promise((resolve, reject) => {
                performance.mark('processData Promise start');
                if (data.boundary === null) {
                    return resolve(c.text('Invalid boundary in content-type header', 500));
                }
                let buffer = Buffer.alloc(0);
                let callback = (chunk: Buffer) => {
                    performance.mark('processData callback start');
                    try {
                        buffer = Buffer.concat([buffer, chunk]);
                        const parts = splitDataByBoundary(buffer, data.boundary);
                        let contentType = Buffer.from('\r\nContent-Type: image/jpeg');
                        if (parts.length > 1) {
                            if (parts[0].indexOf(contentType) > -1) {
                                log(c, 'debug', 'Content-Type found at index:', parts[0].indexOf(contentType), 'of', parts[0].length);
                                let splitData = splitDataByNewLine(parts[0]);
                                let chunkLengths = splitData.map(v => v.length);
                                let config = { writer, x, y, boundary: data.boundary, chunkLengths, canvas, ctx, width: data.width, height: data.height, overlapx: data.overlapx, overlapy: data.overlapy };
                                let frameData = Buffer.concat(splitData.slice(4).map(v => Buffer.concat([v, Buffer.from('\r\n')])));
                                let bytesWritten = 0;
                                if (!c.req.query('full')) {
                                    const roi = {
                                        x: (config.x * 1920) - (config.overlapx * config.x),   // X-coordinate of the top-left corner of the ROI
                                        y: (config.y * 1080) - (config.overlapy * config.y),   // Y-coordinate of the top-left corner of the ROI
                                        width: 1920,  // Width of the ROI
                                        height: 1080, // Height of the ROI
                                    };
                                    loadImage(frameData).then(async (image) => {
                                        config.ctx.clearRect(0, 0, roi.width, roi.height);
                                        config.ctx.drawImage(image, -roi.x, -roi.y);
                                        let croppedFrame = config.canvas.toBuffer('image/jpeg');
                                        let boundaryBuffer = Buffer.from(`--${config.boundary}\r\nContent-Type: image/jpeg\r\nContent-Length: ${croppedFrame.length}\r\n\r\n`);
                                        let data = Buffer.concat([boundaryBuffer, croppedFrame]);
                                        if (data.length > config.writer.desiredSize) {
                                            log(c, 'warn', 'Dropping frame due to insufficient queue size');
                                            return;
                                        }
                                        bytesWritten += data.length;
                                        log(c, 'debug', 'loadImage: Calling writeToStream')
                                        await writeToStream(c, data, config.writer, 65536);
                                        const bytes_needed = 4096 - (bytesWritten % 4096);
                                        const BACKSPACE = 8;
                                        config.writer.write(new Uint8Array(bytes_needed).fill(BACKSPACE));
                                    }).catch((err) => {
                                        if (err instanceof Error) log(c, 'error', 'Error 10: Error loading image:', err.stack);
                                    });
                                } else {
                                    let boundaryBuffer = Buffer.from(`--${config.boundary}\r\nContent-Type: image/jpeg\r\nContent-Length: ${frameData.length}\r\n\r\n`);
                                    let data = Buffer.concat([boundaryBuffer, frameData]);
                                    if (data.length > config.writer.desiredSize) {
                                        log(c, 'warn', 'Dropping frame due to insufficient queue size');
                                        return;
                                    }
                                    bytesWritten += data.length;
                                    writeToStream(c, data, config.writer, 65536);
                                    const bytes_needed = 4096 - (bytesWritten % 4096);
                                    const BACKSPACE = 8;
                                    config.writer.write(new Uint8Array(bytes_needed).fill(BACKSPACE));
                                }
                            }
                            buffer = parts[1];
                        }
                    } catch (err) {
                        if (err instanceof Error) {
                            log(c, 'error', 'Callback Error:', err);
                            return resolve(c.text(err.message, 500));
                        }
                    }
                    performance.mark('processData callback end');
                    performance.measure('processData callback start to end', 'processData callback start', 'processData callback end');
                };
                try {
                    res.setMaxListeners(0);
                    process.addListener('uncaughtException', (error: Error) => {
                        if (error.message != 'Premature close') {
                            log(c, 'error', 'Caught Uncaught Exception:', error);
                        }
                        res.removeListener('data', callback);
                    });
                    res.addListener('data', callback);
                    res.addListener('error', (e) => {
                        log(c, 'error', e);
                        res.removeListener('data', callback)
                    });
                    res.addListener('close', () => {
                        res.removeListener('data', callback)
                    });
                    res.addListener('end', () => {
                        res.removeListener('data', callback)
                    });
                    log(c, 'log', 'PD3: About to resolve with readable');
                    resolve(new Response(readable, {
                        headers: Object.entries(res.headers) as [string, string][],
                        status: res.statusCode,
                        statusText: res.statusMessage,
                    }));
                } catch (e) {
                    log(c, 'error', e);
                    res.removeListener('data', callback);
                }
                performance.measure('processData Promise', 'processData Promise start', 'processData Promise end');
                performance.mark('processData Promise end');
                return;
            });
        }
        function splitDataByBoundary(data: Buffer, boundary: string): Buffer[] {
            try {
                let boundaryBuffer = Buffer.from(`\r\n--${boundary}`);
                let boundaryIndex = data.indexOf(boundaryBuffer);
                if (boundaryIndex == -1) {
                    return [data];
                }
                return [
                    data.subarray(0, boundaryIndex), // Current part before the boundary
                    ...splitDataByBoundary(data.subarray(boundaryIndex + boundaryBuffer.length), boundary), // Remaining parts
                ];
            } catch (err) {
                return [];
            }
        }
        function splitDataByNewLine(data: Buffer): Buffer[] {
            try {
                let newLineBuffer = Buffer.from(`\r\n`);
                let boundaryIndex = data.indexOf(newLineBuffer);
                if (boundaryIndex == -1) {
                    return [data];
                }
                return [
                    data.subarray(0, boundaryIndex), // Current part before the boundary
                    ...splitDataByNewLine(data.subarray(boundaryIndex + newLineBuffer.length)), // Remaining parts
                ];
            } catch (err) {
                return [];
            }
        }
        async function writeToStream(c: Context, data: Buffer, writer: WritableStreamDefaultWriter, chunkSize: number = 4) {
            try {
                let length = data.length;
                let chunk = data.subarray(0, chunkSize);
                log(c, 'debug', 'writeToStream called and running.', chunk.length, writer.desiredSize);
                await writer.ready
                if (writer.desiredSize > 0) {
                    await writer.write(chunk).then(() => log(c, 'debug', 'Successfully wrote chunk to writer'));
                } else {
                    log(c, 'warn', 'Dropped chunk due to stream queue being full')
                }
                if (length > chunkSize) {
                    writeToStream(c, data.subarray(chunk.length, length), writer, chunkSize);
                }
            } catch (err) {
                if (err instanceof Error && err.message != 'Premature close') {
                    log(c, 'error', err);
                } else {
                    log(c, 'error', err);
                    throw err;
                }
            }
        }

        function initCamera(doc: CameraDocument) {
            return new Promise(async (resolve, reject) => {
                let dc = new DigestClient(doc.get('user'), doc.get('pass'));
                if (doc.get('make') == 'AXIS') {
                    let requestURL = `http://${doc.get('host')}:${doc.get('port')}/axis-cgi/mjpg/video.cgi?fps=${doc.get('fps')}`;
                    dc.request(requestURL, (res: IncomingMessage) => {
                        incomingMessages[doc.get('id')] = res;
                        const boundaryMatch = (res.headers['content-type'] || '').match(/boundary=([^;]+)/);
                        let boundary = boundaryMatch ? boundaryMatch[1] : null;
                        let buffer: Buffer;
                        if (res.statusCode == 200) {
                            let callback: (chunk: Buffer) => void;
                            callback = (chunk: Buffer) => {
                                try {
                                    if (buffer) {
                                        buffer = Buffer.concat([buffer, chunk]);
                                    } else {
                                        buffer = chunk;
                                    }
                                    const parts = splitDataByBoundary(buffer, boundary as string);
                                    let contentType = Buffer.from('\r\nContent-Type: image/jpeg');
                                    if (parts.length > 1) {
                                        if (parts[0].indexOf(contentType) > -1) {
                                            let splitData = splitDataByNewLine(parts[0]);
                                            let frame = Buffer.concat(splitData.slice(4).map(v => Buffer.concat([v, Buffer.from('\r\n')])));
                                            loadImage(frame).then((img) => {
                                                let width = img.width;
                                                let height = img.height;
                                                let canvas = createCanvas(266, height / (width / 266));
                                                let ctx = canvas.getContext('2d');
                                                ctx.drawImage(img, 0, 0, 266, height / (width / 266));
                                                let thumbnail = canvas.toDataURL();
                                                doc.modify(docData => {
                                                    docData.thumbnail = thumbnail;
                                                    docData.maxX = Math.ceil(width / 1920);
                                                    docData.maxY = Math.ceil(height / 1080);
                                                    docData.width = width;
                                                    docData.height = height;
                                                    docData.boundary = boundary;
                                                    docData.overlapx = ((docData.maxX * 1920) - width) / (docData.maxX - 1);
                                                    docData.overlapy = ((docData.maxY * 1080) - height) / (docData.maxY - 1);
                                                    return docData;
                                                })
                                                res.off('data', callback);
                                            });
                                        }
                                        buffer = parts[1];
                                    }
                                } catch (err) {
                                    if (err instanceof Error) {
                                        console.error(err);
                                        res.off('data', callback);
                                    }
                                }
                            };
                            res.on('data', callback);
                        } else {
                            console.log(res.headers)
                            console.warn(res.statusCode, res.statusMessage, res.headers['www-authenticate']);
                        }
                    }).end();
                }
            })
        }

        app.get('/api/supportedMake', (c: Context) => c.json({
            results: [
                { name: 'AXIS', value: 'AXIS' }
            ], success: true
        }));

        app.get('/api/camera', async (c: Context) => {
            let id = c.req.query('id');
            if (id) {
                let data = await db.cameras.findOne({ selector: { id } }).exec(true);
                delete data.pass;
                return c.json(data);
            } else {
                let data = await db.cameras.find().exec();
                return c.json(data);
            }
        });

        app.post('/api/camera', async (c: Context) => {
            const body = await c.req.json();
            let camDoc = await db.cameras.insert({
                id: randomUUID(),
                name: body.name || [body.make, body.model].join(' '),
                host: body.host || '',
                port: body.port || 80,
                make: body.make || '',
                model: body.model || '',
                user: body.user || '',
                pass: body.pass || '',
                fps: body.fps || 30
            });
            // make connection to camera
            // calculate and save docData.maxX and docData.maxY
            initCamera(camDoc);
            let data = { ...camDoc };
            delete data.pass;
            return c.json(camDoc);
        });

        app.patch('/api/camera', async (c: Context) => {
            const body = await c.req.json();
            let foundDoc = await db.cameras.findOne({ selector: { id: body.id } }).exec();
            let camDoc = await foundDoc.modify(docData => {
                if (body?.name) docData.name = body?.name;
                if (body?.host) docData.host = body?.host;
                if (body?.port) docData.port = body?.port;
                if (body?.make) docData.make = body?.make;
                if (body?.model) docData.model = body?.model;
                if (body?.user) docData.user = body?.user;
                if (body?.changePass) docData.pass = body.pass;
                if (body?.fps) docData.fps = body?.fps;
                return docData;
            });
            if (body?.host || body?.port || body?.user || body?.changePass || body?.fps) {
                // reconnect to camera
                // recalculate and save docData.maxX and docData.maxY
                incomingMessages[camDoc.get('id')].destroy();
                delete incomingMessages[camDoc.get('id')];
                initCamera(camDoc);
            }
            return c.json(camDoc);
        });

        app.delete('/api/camera', async (c: Context) => {
            let id = c.req.query('id');
            if (id) {
                console.log('User wishes to delete', id);
                let camDoc = await db.cameras.findOne({ selector: { id } }).exec();
                if (camDoc) {
                    camDoc.remove();
                    incomingMessages[id].destroy();
                    delete incomingMessages[id];
                }
                return c.text('OK')
            }
            return c.text('Bad request', 400);
        });

        app.get('/', (c: Context) => {
            return c.html(<html lang="en-us">
                <head>
                    <meta charset="utf-8" />
                    <title>MJPEG Splitter</title>
                    <meta name="description" content="" />
                    <meta name="viewport" content="width=device-width, initial-scale=1" />
                    <script src="https://cdn.jsdelivr.net/npm/jquery@3.7.1/dist/jquery.min.js"></script>
                    <script src="https://cdn.jsdelivr.net/npm/fomantic-ui/dist/semantic.min.js"></script>
                    <link href="https://cdn.jsdelivr.net/npm/fomantic-ui/dist/semantic.min.css" rel="stylesheet" />
                </head>

                <body class="ui container">
                    <div class="ui top menu">
                        <div class="ui item">
                            <h1 class="ui header">MJPEG Splitter</h1>
                        </div>
                    </div>
                    <div class="ui link centered cards">
                        <div class="card" id="addCameraBtn">
                            <div class="center aligned image">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" height="56" width="56">
                                    <path d="M19,13H13V19H11V13H5V11H11V5H13V11H19V13Z" />
                                </svg>
                            </div>
                            <div class="content">
                                <div class="center aligned header">Add Camera</div>
                            </div>
                        </div>
                    </div>
                    <script>{html`
const cameras = { };
function updateCameraCard(data) {
    cameras[data.id] = data;
    $(\`[data-id=\${data.id}] img\`)?.attr('src', data?.thumbnail);
    $(\`[data-id=\${data.id}] header\`).text(data?.name);
    $(\`[data-id=\${data.id}] meta\`).text(\`\${data?.host}:\${data?.port}\`);
    $(\`[data-id=\${data.id}] description\`).text(\`\${data?.make} \${data?.model}\`);
}
function addCameraCard(data) {
    cameras[data.id] = data;
    let cardWrapper = $(\`<div data-id="\${data?.id}" class="card"></div>\`);
    let cardImagePlaceholder = $(\`<div class="ui placeholder"></div>\`);
    let cardImageWrapper = $(\`<div class="center aligned image"></div>\`);
    let cardImage = $(\`<img src="\${data?.thumbnail ? data.thumbnail : ''}" />\`)
    let cardContentWrapper = $(\`<div class="content"></div>\`);
    let cardHeader = $(\`<div class="center aligned header">\${data?.name}</div>\`);
    let cardMeta = $(\`<div class="center aligned meta">\${data?.host}:\${data?.port}</div>\`);
    let cardDescription = $(\`<div class="center aligned description">\${data?.make} \${data?.model}</div>\`);
    if (!data?.thumbnail) {
        cardWrapper.append(cardImagePlaceholder)
    } else {
        cardWrapper.append(cardImageWrapper.append(cardImage))
    }
    cardContentWrapper.append(cardHeader).append(cardMeta).append(cardDescription);
    cardWrapper.append(cardContentWrapper).on('click', () => {
        let urls = [];
        for (let x = 0; x < data.maxX; x++) {
            for (let y = 0; y < data.maxY; y++) {
                urls.push(\`<div class="item"><i class="right triangle icon"></i><div class="top aligned content"><pre style="margin: 0;">\${document.location.origin}/mjpeg?id=\${data.id}&x=\${x}&y=\${y}</pre></div></div>\`)
            }
        }
        $.modal({
            title: \`\${data?.name}\`,
            class: 'info',
            classTitle: 'center aligned',
            closeIcon: true,
            content: \`<div class="ui attached message"><div class="header">\${data?.make} \${data?.model}</div><p>\${data?.host}:\${data?.port}</p></div>
            <div class="ui attached segment" id="cameraInfo" style="overflow-y: auto; max-height: 50vh;">
                <h3 class="header">1080p MJPEG Stream URLs</h3>
                <p>In Remote Builder, for each URL listed below, create a new Camera with the model set to Generic MJPEG camera. Copy and paste these URLs into source text box.</p>
                <div class="ui list">
                    \${urls.join('\\n')}
                </div>
                <div></div>
            </div>\`,
            onShow: () => {
                    $('.ui.tiny.modal').removeClass('tiny');
            },
            actions: [{
                text: 'Edit',
                class: 'gray',
                click: () => {
                    $.modal({
                        title: 'Edit Camera',
                        class: 'info',
                        classTitle: 'center aligned',
                        closeIcon: true,
                        content: \`<form id="editCameraForm" class="ui form">
                <input type="hidden" name="id" />
                <div class="field">
                    <label>Name</label>
                    <input type="text" name="name" placeholder="Camera Name" />
                </div>
                <div class="four fields">
                    <div class="eight wide field">
                        <label>IP Address or Host Name</label>
                        <input type="text" name="host" placeholder="e.g. 192.168.1.100" />
                    </div>
                    <div class="four wide field">
                        <label>Port Number</label>
                        <input type="text" name="port" placeholder="80" />
                    </div>
                    <div class="four wide field">
                        <label title="Frames Per Second">FPS</label>
                        <input type="number" name="fps" placeholder="30" title="Frames Per Second" />
                    </div>
                </div>
                <div class="two fields">
                    <div class="field">
                        <label>Manufacturer Name</label>
                        <div class="ui selection dropdown">
                            <input type="hidden" name="make">
                            <i class="dropdown icon"></i>
                            <div class="default text">AXIS</div>
                            <div class="scrollhint menu">
                            </div>
                        </div>
                    </div>
                    <div class="field">
                        <label>Model Number</label>
                        <input type="text" name="model" placeholder="Q3819-PVE" />
                    </div>
                </div>
                <div class="four fields">
                    <div class="eight wide field">
                        <label>Username</label>
                        <input type="text" name="user" placeholder="admin" />
                    </div>
                    <div class="five wide field">
                        <label>Password</label>
                        <input type="password" name="pass" placeholder="********" title="Password will not change unless you check Change Password" />
                    </div>
                    <div class="three wide field">
                        <div class="ui fluid checkbox">
                            <input type="checkbox" name="changePass" />
                            <label>Change Password</label>
                        </div>
                    </div>
                </div>
                </form>\`,
                        actions: [{
                            text: 'Save',
                            class: 'green approve'
                        }, {
                            text: 'Delete',
                            class: 'red',
                            click: () => {
                                let id = $('#editCameraForm').form('get value', 'id');
                                $.modal('confirm', 'Are you sure you want to delete?', (choice) => {
                                    if (choice) {
                                        fetch(\`/api/camera?id=\${id}\`, { method: 'DELETE' })
                                            .then(() => {
                                                $(\`[data-id=\${id}]\`).remove();
                                                $.modal('hide');
                                            })
                                    }
                                })
                            }
                        }, {
                            text: 'Cancel',
                            class: 'gray deny'
                        }],
                        onShow: () => {
                            $('.ui.tiny.modal').removeClass('tiny');
                            let id = data.id;
                            $('#editCameraForm .ui.dropdown').dropdown({
                                apiSettings: {
                                    url: '/api/supportedMake'
                                }
                            }).dropdown('queryRemote', '', () => {
                                $('#editCameraForm').form({
                                    fields: {
                                        name: {
                                            identifier: 'name',
                                            rules: [
                                                {
                                                    type: 'empty',
                                                    prompt: 'Please give your camera a name'
                                                }
                                            ]
                                        },
                                        host: {
                                            identifier: 'host',
                                            rules: [
                                                {
                                                    type: 'empty',
                                                    prompt: 'Please enter the host name or IP address of your camera'
                                                }
                                            ]
                                        },
                                        port: {
                                            identifier: 'port',
                                            rules: [
                                                {
                                                    type: 'empty',
                                                    prompt: 'Please enter the port number of your camera'
                                                }
                                            ]
                                        },
                                        fps: {
                                            identifier: 'fps',
                                        },
                                        make: {
                                            identifier: 'make',
                                        },
                                        model: {
                                            identifier: 'model',
                                        },
                                        user: {
                                            identifier: 'user',
                                            rules: [
                                                {
                                                    type: 'empty',
                                                    prompt: 'Please enter a username for connecting to the camera'
                                                }
                                            ]
                                        },
                                    }
                                }).form('set values', {
                                    id,
                                    name: cameras[id].name,
                                    host: cameras[id].host,
                                    port: cameras[id].port,
                                    make: cameras[id].make,
                                    model: cameras[id].model,
                                    user: cameras[id].user,
                                    fps: cameras[id].fps
                                });
                            });
                        },
                        onApprove: (el) => {
                            event.preventDefault();
                            event.stopPropagation();
                            if ($('#editCameraForm').form('is valid')) {
                                let data = $('#editCameraForm').form('get values');
                                fetch('/api/camera', { method: 'PATCH', body: JSON.stringify(data) })
                                    .then(res => {
                                        if (res.status == 200) {
                                            updateCameraCard(data);
                                            $.modal('hide');
                                        }
                                    });
                            } else {
                                $('#editCameraForm').form('validate form');
                            }
                            return false;
                        }
                    }).modal('show');
                }
            }, {
                text: 'Close',
                class: 'gray approve'
            }]
        }).modal('show');
    });
    $('.ui.link.cards').append(cardWrapper);
}
$(document).ready(() => {
    $('#addCameraBtn').on('click', () => {
        let modal;
        modal = $.modal({
            title: 'Add Camera',
            class: 'info',
            classTitle: 'center aligned',
            closeIcon: true,
            content: \`<form id="addCameraForm" class="ui form">
    <div class="field">
        <label>Name</label>
        <input type="text" name="name" placeholder="Camera Name" />
    </div>
    <div class="four fields">
        <div class="eight wide field">
            <label>IP Address or Host Name</label>
            <input type="text" name="host" placeholder="e.g. 192.168.1.100" />
        </div>
        <div class="four wide field">
            <label>Port Number</label>
            <input type="text" name="port" placeholder="80" />
        </div>
        <div class="four wide field">
            <label title="Frames Per Second">FPS</label>
            <input type="number" name="fps" placeholder="30" title="Frames Per Second" />
        </div>
    </div>
    <div class="two fields">
        <div class="field">
            <label>Manufacturer Name</label>
            <div class="ui selection dropdown">
                <input type="hidden" name="make">
                <i class="dropdown icon"></i>
                <div class="default text">AXIS</div>
                <div class="scrollhint menu">
                </div>
            </div>
        </div>
        <div class="field">
            <label>Model Number</label>
            <input type="text" name="model" placeholder="Q3819-PVE" />
        </div>
    </div>
    <div class="two fields">
        <div class="field">
            <label>Username</label>
            <input type="text" name="user" placeholder="admin" />
        </div>
        <div class="field">
            <label>Password</label>
            <input type="password" name="pass" placeholder="********" />
        </div>
    </div>
</form>\`,
            actions: [{
                text: 'Add',
                class: 'green approve'
            }, {
                text: 'Cancel',
                class: 'red deny'
            }],
            onShow: () => {
                $('.ui.tiny.modal').removeClass('tiny');
                $('#addCameraForm .ui.dropdown').dropdown({
                    apiSettings: {
                        url: '/api/supportedMake'
                    }
                })
                $('#addCameraForm').form({
                    fields: {
                        name: {
                            identifier: 'name',
                            rules: [
                                {
                                    type: 'empty',
                                    prompt: 'Please give your camera a name'
                                }
                            ]
                        },
                        host: {
                            identifier: 'host',
                            rules: [
                                {
                                    type: 'empty',
                                    prompt: 'Please enter the host name or IP address of your camera'
                                }
                            ]
                        },
                        port: {
                            identifier: 'port',
                            rules: [
                                {
                                    type: 'empty',
                                    prompt: 'Please enter the port number of your camera'
                                }
                            ]
                        },
                        fps: {
                            identifier: 'fps',
                        },
                        make: {
                            identifier: 'make',
                        },
                        model: {
                            identifier: 'model',
                        },
                        user: {
                            identifier: 'user',
                            rules: [
                                {
                                    type: 'empty',
                                    prompt: 'Please enter a username for connecting to the camera'
                                }
                            ]
                        },
                        pass: {
                            identifier: 'pass',
                            rules: [
                                {
                                    type: 'empty',
                                    prompt: 'Please enter a password for connecting to the camera'
                                }
                            ]
                        }
                    }
                })
            },
            onApprove: (el) => {
                event.preventDefault();
                event.stopPropagation();
                if ($('#addCameraForm').form('is valid')) {
                    let data = $('#addCameraForm').form('get values');
                    fetch('/api/camera', { method: 'POST', body: JSON.stringify(data) })
                        .then(async res => {
                            if (res.status == 200) {
                                addCameraCard(await res.json());
                                modal.modal('hide');
                            }
                        });
                    return false;
                } else {
                    $('#addCameraForm').form('validate form');
                    return false;
                }
            }
        }).modal('show')
    });
    fetch('/api/camera')
        .then(res => res.status == 200 ? res.json() : undefined)
        .then(data => {
            if (data) {
                data.forEach((v) => addCameraCard(v))
            }
        });
});
`}</script>
                </body>
            </html>)
        })
        db.cameras.find().exec().then(docs => {
            docs.forEach(doc => {
                initCamera(doc);
            })
        })
        let port = Number(process.argv[2]);
        serve({ fetch: app.fetch, port: !isNaN(port) ? port : 3000 }, (info) => {
            console.log(`Listening on http://localhost:${info.port}`);
        });
    } catch (err) {
        if (err instanceof Error) {
            console.error(err.stack);
        }
    }
})()
} else {
    switch (workerData.type) {

    }
}