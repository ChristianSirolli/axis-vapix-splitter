import { readFile, writeFile } from "fs";
console.log(process.env.npm_package_version)
function patch(file) {
    readFile(file, 'utf8', function (err, data) {
        if (err) {
            return console.log(err);
        }
        var result = data.replaceAll(/nativeBinding = require\("\.\/(.*?.node)"\);/g, `nativeBinding = createRequire(__dirname)(join(__dirname, "$1"))`);

        writeFile(file, result, 'utf8', function (err) {
            if (err) return console.log(err);
        });
    });
}
patch(`./build/${process.argv[2]}/init.cjs`);