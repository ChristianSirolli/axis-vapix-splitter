import { createHash } from 'node:crypto';
import { request } from 'node:http';
export class DigestClient {
    username;
    password;
    nonceCount = 1;
    _request;
    realm;
    nonce;
    algorithm;
    qop;
    opaque;
    stale;
    userhash;
    charset;
    constructor(username, password) {
        this.username = username.replaceAll('"', '');
        this.password = password;
    }
    request(urlOrOptions, optionsOrCallback, callback) {
        const $this = this;
        let callbackRouter = (res) => {
            let currentCallback;
            let currentOptions;
            if (typeof optionsOrCallback == 'function') {
                currentCallback = optionsOrCallback;
            }
            else if (typeof callback == 'function') {
                currentCallback = callback;
            }
            if (optionsOrCallback && typeof optionsOrCallback != 'function') {
                currentOptions = optionsOrCallback;
            }
            else if (urlOrOptions && typeof urlOrOptions != 'string' && !(urlOrOptions instanceof URL)) {
                currentOptions = urlOrOptions;
            }
            if (res.statusCode == 401) {
                let url;
                if (typeof urlOrOptions == 'string' || urlOrOptions instanceof URL) {
                    url = urlOrOptions;
                }
                $this._handle401(url, res, currentOptions, currentCallback);
            }
            else if (currentCallback) {
                currentCallback(res);
            }
        };
        if (typeof urlOrOptions == 'string' || urlOrOptions instanceof URL) {
            if (typeof optionsOrCallback != 'function') {
                $this._request = request(urlOrOptions, optionsOrCallback, callbackRouter);
                return $this._request;
            }
        }
        $this._request = request(urlOrOptions, callbackRouter);
        return $this._request;
    }
    _genEntityBody(res) {
        return new Promise((resolve, reject) => {
            let entityBody;
            let dataHandler = () => {
                res.setEncoding('utf-8');
                entityBody = Object.entries(res.headers).map(v => v.join(': ')).join('\r\n');
                entityBody += '\r\n';
                let chunk;
                while (null !== (chunk = res.read())) {
                    entityBody += res.read();
                }
            };
            res.on('readable', dataHandler);
            res.on('end', () => {
                res.off('data', dataHandler);
                entityBody += '\r\n';
                entityBody += res.rawTrailers.join('\r\n');
                resolve(entityBody);
            });
            res.on('close', () => {
                res.off('data', dataHandler);
                resolve(entityBody);
            });
            res.on('error', (err) => {
                res.off('data', dataHandler);
                reject(err);
            });
        });
    }
    _handle401(url, res, options, callback) {
        const $this = this;
        $this._genDigestAuth(res.headers['www-authenticate'], options?.method || 'GET', url.toString(), res).then(digestStr => {
            let _request = $this.request.bind($this);
            let headers = {
                ...options?.headers,
                Authorization: digestStr
            };
            if (url) {
                $this._request = _request(url, { ...options, headers }, callback);
            }
            else {
                $this._request = _request({ ...options, headers }, callback);
            }
            $this.end();
        });
    }
    async _genDigestAuth(authStr, method, digestURI, res) {
        try {
            let ha1, ha2, response, authParams, nc, cnonce, digestStr, entityBody, domain, stale, alg, userhash, selectedQop, digestObj;
            authParams = new URLSearchParams(authStr?.replace('Digest ', '')?.replaceAll(', ', '&')?.replaceAll('"', ''));
            this.realm = authParams.get('realm');
            domain = authParams.get('domain');
            this.nonce = authParams.get('nonce');
            this.opaque = authParams.get('opaque');
            stale = authParams.get('stale');
            this.stale = stale ? stale?.toLowerCase() != 'false' : false;
            this.algorithm = authParams.get('algorithm');
            alg = this.algorithm.toLowerCase().replace('-sess', '').replace('-', '');
            this.qop = authParams.get('qop').split(',') || ['auth'];
            this.charset = (authParams.get('charset')?.toLowerCase() || 'utf-8');
            userhash = authParams.get('userhash');
            this.userhash = userhash ? userhash.toLowerCase() == 'true' : false;
            if (this.userhash) {
                this.username = createHash(alg).update(this.username, this.charset).digest("hex");
            }
            cnonce = new Date().valueOf().toString(16);
            nc = (this.nonceCount++).toString(16).padStart(8, '0');
            ha1 = createHash(alg).update([this.username, this.realm, this.password].join(':'), this.charset).digest("hex");
            if (this.algorithm.match(/-sess$/)) {
                ha1 = createHash(alg).update([ha1, this.nonce, cnonce].join(':'), this.charset).digest("hex");
            }
            selectedQop = 'auth';
            if (this.qop.includes('auth-int')) {
                selectedQop = 'auth-int';
                entityBody = createHash(alg).update((await this._genEntityBody(res)), this.charset).digest("hex");
                ha2 = createHash(alg).update([method.toUpperCase(), digestURI, entityBody].join(':'), this.charset).digest("hex");
            }
            else {
                ha2 = createHash(alg).update([method.toUpperCase(), digestURI].join(':'), this.charset).digest("hex");
            }
            response = ['"', createHash(alg).update([ha1, this.nonce, nc, cnonce, selectedQop, ha2].join(':'), this.charset).digest("hex"), '"'].join('');
            digestObj = {
                response,
                username: `"${this.username}"`,
                realm: `"${this.realm}"`,
                uri: `"${digestURI}"`,
                qop: selectedQop,
                cnonce: `"${cnonce}"`,
                nc,
                userhash: `"${this.userhash}"`,
                nonce: `"${this.nonce}"`,
                algorithm: this.algorithm,
            };
            if (false && !this.userhash) {
                delete digestObj.username;
                digestObj['username*'] = this.username;
            }
            if (this.opaque) {
                digestObj.opaque = `"${this.opaque}"`;
            }
            digestStr = `Digest ${Object.entries(digestObj).map(v => v.join('=')).join(', ')}`;
            return digestStr;
        }
        catch (e) {
            throw e;
        }
    }
    end(chunkOrCb, encodingOrCB, cb) {
        if (cb) {
            return this._request.end(chunkOrCb, encodingOrCB, cb);
        }
        else if (encodingOrCB) {
            return this._request.end(chunkOrCb, encodingOrCB);
        }
        return this._request.end(chunkOrCb);
    }
    aborted;
    host;
    protocol;
    reusedSocket;
    maxHeadersCount;
    method;
    path;
    abort() {
        this._request.abort();
    }
    ;
    onSocket(socket) {
        this._request.onSocket(socket);
    }
    ;
    setTimeout(timeout, callback) {
        return this._request.setTimeout(timeout, callback);
    }
    ;
    setNoDelay(noDelay) {
        this._request.setNoDelay(noDelay);
    }
    ;
    setSocketKeepAlive(enable, initialDelay) {
        this._request.setSocketKeepAlive(enable, initialDelay);
    }
    ;
    getRawHeaderNames() {
        return this._request.getRawHeaderNames();
    }
    ;
    addListener(event, listener) {
        return this._request.addListener(event, listener);
    }
    ;
    on(event, listener) {
        return this._request.on(event, listener);
    }
    once(event, listener) {
        return this._request.once(event, listener);
    }
    ;
    prependListener(event, listener) {
        return this._request.prependListener(event, listener);
    }
    ;
    prependOnceListener(event, listener) {
        return this._request.prependOnceListener(event, listener);
    }
    ;
}
