# mjpeg-splitter

Creates an HTTP server (via Hono.js) that splits an MJPEG stream into separate 1080p MJPEG streams.

Currently only supports Axis' VAPIX API.

## See also

* [axis-discovery](https://www.npmjs.com/package/axis-discovery) (Apache-2.0)
* [axis-configuration](https://www.npmjs.com/package/axis-configuration) (Apache-2.0)
* [axis-snapshop](https://www.npmjs.com/package/axis-snapshot) (Apache-2.0)
* [axis-maintenance](https://www.npmjs.com/package/axis-maintenance) (Apache-2.0)

## Known Bugs
None at the moment

## Resources
https://medium.com/craftsmenltd/building-a-cross-platform-background-service-in-node-js-791cfcd3be60

https://github.com/pmq20/node-packer
https://github.com/crosstool-ng/crosstool-ng
https://crosstool-ng.github.io/docs/

https://nodejs.org/api/single-executable-applications.html
https://dev.to/chad_r_stewart/compile-a-single-executable-from-your-node-app-with-nodejs-20-and-esbuild-210j

### DEB
https://www.internalpointers.com/post/build-binary-deb-package-practical-guide
https://chmodcommand.com/chmod-775/